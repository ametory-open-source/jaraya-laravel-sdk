<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Ametory\JarayaLaravelSDK\Facades\Customer;

class TestController extends Controller
{
    public function test(Request $request)
    {
        $customer = Customer::get($request->all());
        return response()->json($customer->json(), $customer->status());
    }

}
