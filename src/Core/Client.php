<?php

namespace Ametory\JarayaLaravelSDK\Core;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
class Client {

    private $client;
    private $headers;
    private $baseUrl = "";
    private $prefix = "/api/v2/Public";
    public $response;

    public function __construct($headers = [], $form = false) 
    {
        if (config("jaraya.environment") == Env::STAGING) {
            $this->baseUrl = config("jaraya.base_url_staging");
        } else  if (config("jaraya.environment") == Env::PRODUCTION) {
            $this->baseUrl = config("jaraya.base_url_prod");
        } else {
            throw new \Exception("Please set up env first");
        }
        $this->headers = [
            'API-Key' => config('jaraya.api_key'),
            'API-Secret' => config('jaraya.api_secret'),
            'Content-Type' => 'application/json',
        ];

        $this->client = Http::withHeaders( $this->headers );
        if ($form) {
            $this->client = $this->client->asForm();
        }
    }

    public function setHeaders($headers)
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function get($path, $params = [])
    {
        $response = $this->client->get($this->baseUrl . $this->prefix . $path, $params);
        $this->response = $response;
        return $response;
    }

    public function post($path, $data = [])
    {
        $response = $this->client->post($this->baseUrl . $this->prefix . $path, $data);
        $this->response = $response;
        return $response;
    }

}