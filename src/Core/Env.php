<?php

namespace Ametory\JarayaLaravelSDK\Core;

final class Env
{
    public const STAGING = 'STAGING';
    public const PRODUCTION = 'PRODUCTION';
}