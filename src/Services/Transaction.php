<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Transaction {

    public function get($params) {
        return Client::get("/Transaction", $params);
    }
    
    public function create($params) {
        return Client::post("/Transaction", $params);
    }
}