<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Customer {

    public function get($params) {
        return Client::get("/Customer", $params);
    }
    
    public function create($params) {
        return Client::post("/Customer", $params);
    }
}