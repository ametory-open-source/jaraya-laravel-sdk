<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Auth {

    public function registration($params) {
        return Client::post("/Registration", $params);
    }
    
    public function verification($params) {
        return Client::post("/Verification", $params);
    }
}