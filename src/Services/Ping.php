<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Ping {
    
    public function send() {
        return Client::get("/ping");
    }
}