<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Account {

    public function get($params) {
        return Client::get("/Account", $params);
    }
    
    public function type() {
        return Client::get("/Account/Type");
    }
}