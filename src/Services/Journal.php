<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Journal {

    public function get($params) {
        return Client::get("/Journal", $params);
    }
    
    public function create($params) {
        return Client::post("/Journal", $params);
    }
}