<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Company {
    public function category($params) {
        return Client::get("/CompanyCategory", $params);
    }
}