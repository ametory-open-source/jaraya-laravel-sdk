<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Employee {

    public function get($params) {
        return Client::get("/Employee", $params);
    }
    
    public function sync($params) {
        return Client::post("/Employee/Sync", $params);
    }

    public function syncAll($params) {
        return Client::post("/Employee/SyncAll", $params);
    }

    public function payroll($params) {
        return Client::post("/Employee/Payroll", $params);
    }
}