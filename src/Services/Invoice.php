<?php

namespace Ametory\JarayaLaravelSDK\Services;

use Ametory\JarayaLaravelSDK\Facades\Client;

class Invoice {

    public function get($params) {
        return Client::get("/Invoice", $params);
    }
    
    public function create($params) {
        return Client::post("/Invoice", $params);
    }
}