<?php

return [
    'environment' => env("JARAYA_API_ENV", "STAGING"),
    'api_key' => env("JARAYA_API_KEY"),
    'api_secret' => env("JARAYA_API_SECRET"),
    'base_url_staging' => 'https://apiv2-jaraya.staging1.ametory.id',
    'base_url_prod' => 'https://api.jaraya.id',

];