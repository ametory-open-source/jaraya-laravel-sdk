<?php

namespace Ametory\JarayaLaravelSDK\Providers;

use Illuminate\Support\ServiceProvider;

class JarayaLaravelSDKProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/jaraya.php' => config_path('jaraya.php'),
        ], 'jaraya');
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/jaraya.php', 'jaraya');
    }
}
