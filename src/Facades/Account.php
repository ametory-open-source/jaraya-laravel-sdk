<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Account as AccountService;
use Illuminate\Support\Facades\Facade;

class Account extends Facade {
    protected static function getFacadeAccessor()
    {
        return AccountService::class;
    }
}