<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Company as CompanyService;
use Illuminate\Support\Facades\Facade;

class Company extends Facade {
    protected static function getFacadeAccessor()
    {
        return CompanyService::class;
    }
}