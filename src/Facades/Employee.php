<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Employee as EmployeeService;
use Illuminate\Support\Facades\Facade;

class Employee extends Facade {
    protected static function getFacadeAccessor()
    {
        return EmployeeService::class;
    }
}