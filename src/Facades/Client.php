<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Core\Client as ClientService;
use Illuminate\Support\Facades\Facade;

class Client extends Facade {
    protected static function getFacadeAccessor()
    {
        return ClientService::class;
    }
}