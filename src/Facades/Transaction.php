<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Transaction as TransactionService;
use Illuminate\Support\Facades\Facade;

class Transaction extends Facade {
    protected static function getFacadeAccessor()
    {
        return TransactionService::class;
    }
}