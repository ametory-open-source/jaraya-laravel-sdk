<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Journal as JournalService;
use Illuminate\Support\Facades\Facade;

class Journal extends Facade {
    protected static function getFacadeAccessor()
    {
        return JournalService::class;
    }
}