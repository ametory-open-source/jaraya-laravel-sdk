<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Customer as CustomerService;
use Illuminate\Support\Facades\Facade;

class Customer extends Facade {
    protected static function getFacadeAccessor()
    {
        return CustomerService::class;
    }
}