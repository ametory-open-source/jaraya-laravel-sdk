<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Invoice as InvoiceService;
use Illuminate\Support\Facades\Facade;

class Invoice extends Facade {
    protected static function getFacadeAccessor()
    {
        return InvoiceService::class;
    }
}