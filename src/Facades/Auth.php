<?php

namespace Ametory\JarayaLaravelSDK\Facades;

use Ametory\JarayaLaravelSDK\Services\Auth as AuthService;
use Illuminate\Support\Facades\Facade;

class Auth extends Facade {
    protected static function getFacadeAccessor()
    {
        return AuthService::class;
    }
}